#include "PDIUSBD12.h"





void D12WriteCommand(uint8 Command){
	D12SetCommandAddr();
	D12ClrWr();
	D12SetPortOut();
	D12SetData(Command);
	D12SetWr();
	D12SetPortIn();

}



uint8 D12ReadByte(void){
	uint8 temp;
	D12SetDataAddr();
	D12ClrRd();
	temp = D12GetData();
	D12SetRd();
	return temp;
}




uint16 D12ReadID(void){
	uint16 id;
	D12WriteCommand(Read_ID);
	id = D12ReadByte();
	id |= ((uint16)D12ReadByte()) << 8;
	return id;

}






