#include <STC89C5xRC.H>
#include "UART.h"
#include "MyType.h"
#include "config.h"


volatile uint8 Sending;


void InitUART(void)
{
    EA = 0;
    TMOD &= 0x0F;
    TMOD |= 0x20;
    SCON = 0x50;
    TH1 = 256 - Fclk / (BitRate * 12 * 16);
    TL1 = 256 - Fclk / (BitRate * 12 * 16);
    PCON |= 0x80;
    ES = 1;
    TR1 = 1;
    REN = 1;
    EA = 1;

}



void UartISR(void) interrupt 4
{
    if(RI)
    {
        RI = 0;
    }
    else
    {
        TI = 0;
        Sending = 0;
    }
}


void UartPutChar(uint8 d)
{
    SBUF = d;
    Sending = 1;

    while(Sending);
}




void Prints(uint8 *pd)
{
    while((*pd) != '\0')
    {
        UartPutChar(*pd);
        pd++;
    }

}


code uint8 HexTable[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};


void PrintShortIntHex(uint16 x)
{
    uint8 i;
    uint8 display_buffer[7];
    display_buffer[6] = 0;
    display_buffer[0] = '0';
    display_buffer[1] = 'x';

    for(i = 5; i >= 2; i--)
    {
        display_buffer[i] = HexTable[(x & 0xf)];
        x >>= 4;
    }

    Prints(display_buffer);
}


