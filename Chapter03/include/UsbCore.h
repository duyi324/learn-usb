#ifndef __USB_CORE_H__
#define __USB_CORE_H__


#include "MyType.h"
#include "PDIUSBD12.h"



void DelayXms(uint16);
void UsbDisconnect(void);
void UsbConnect(void);

void UsbBusSuspend();
void UsbBusReset();
void UsbEp0Out();
void UsbEp0In();
void UsbEp1Out();
void UsbEp1In();
void UsbEp2Out();
void UsbEp2In();





#endif
