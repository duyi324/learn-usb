#ifndef __UART_H__
#define __UART_H__

#include "MyType.h"


void InitUART(void);
void UartPutChar(uint8);
void Prints(uint8 *pd);
void PrintShortIntHex(uint16 x);
void PrintLongInt(uint32);
void Printc(uint8);
void PrintHex(uint8);

void D12ClearBuffer(void);
void D12AcknowledgeSetup(void);

#endif
