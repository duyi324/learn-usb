#include <STC89C5xRC.H>
#include "Key.h"
#include "Led.h"
#include "PDIUSBD12.h"
#include "UART.h"
#include "UsbCore.h"



void main(void)
{
    uint16 id;
    uint8 InterruptSource;

    EA = 1;
    InitKeyboard();
	  InitUART();


    id = D12ReadID();
    Prints("Your D12 chip\'s ID is: ");
    PrintShortIntHex(id);

    if(id == 0x1012)
    {
        Prints(". ID is correct! \r\n\r\n");
    }
    else
    {
        Prints(". ID is incorrect! \r\n\r\n");
    }


    UsbDisconnect();
    UsbConnect();



    while(1)
    {
        if(D12GetIntPin() == 0)
        {
            D12WriteCommand(READ_INTERRUPT_REGISTER);
            InterruptSource = D12ReadByte();

            if(InterruptSource & 0x80)
            {
                UsbBusSuspend();
            }

            if(InterruptSource & 0x40)
            {
                UsbBusReset();
            }

            if(InterruptSource & 0x01)
            {
                UsbEp0Out();
            }

            if(InterruptSource & 0x02)
            {
                UsbEp0In();
            }

            if(InterruptSource & 0x04)
            {
                UsbEp1Out();
            }

            if(InterruptSource & 0x08)
            {
                UsbEp1In();
            }

            if(InterruptSource & 0x10)
            {
                UsbEp2Out();
            }

            if(InterruptSource & 0x20)
            {
                UsbEp2In();
            }
        }
    }

}










