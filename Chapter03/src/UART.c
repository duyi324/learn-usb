#include <STC89C5xRC.H>
#include "UART.h"
#include "MyType.h"
#include "config.h"


volatile uint8 Sending;


void InitUART(void)
{
    EA = 0;
    TMOD &= 0x0F;
    TMOD |= 0x20;
    SCON = 0x50;
    TH1 = 256 - Fclk / (BitRate * 12 * 16);
    TL1 = 256 - Fclk / (BitRate * 12 * 16);
    PCON |= 0x80;
    ES = 1;
    TR1 = 1;
    REN = 1;
    EA = 1;

}



void UartISR(void) interrupt 4
{
    if(RI)
    {
        RI = 0;
    }
    else
    {
        TI = 0;
        Sending = 0;
    }
}


/**************************************************
	函数功能：往串口发送1字节数据
	入口参数：d 要发送的字节数据
	返    回：无
	备    注：无
**************************************************/
void UartPutChar(uint8 d)
{
    SBUF = d;
    Sending = 1;

    while(Sending);
}



/**************************************************
	函数功能：发送一个字符串
	入口参数：pd 要发送的字符串指针
	返    回：无
	备    注：无
**************************************************/
void Prints(uint8 *pd)
{
    while((*pd) != '\0')
    {
        UartPutChar(*pd);
        pd++;
    }

}


code uint8 HexTable[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};



/**************************************************
	函数功能：将短整数按十六进制发送
	入口参数：x 待发送的整数
	返    回：无
	备    注：无
**************************************************/
void PrintShortIntHex(uint16 x)
{
    uint8 i;
    uint8 display_buffer[7];
    display_buffer[6] = 0;
    display_buffer[0] = '0';
    display_buffer[1] = 'x';

    for(i = 5; i >= 2; i--)
    {
        display_buffer[i] = HexTable[(x & 0xf)];
        x >>= 4;
    }

    Prints(display_buffer);
}


/**************************************************
	函数功能：将整数按十进制字符串发送
	入口参数：x 待显示的整数
	返    回：无
	备    注：无
**************************************************/
void PrintLongInt(uint32 x)
{
    int8 i;
    uint8 display_buffer[10];

    for(i = 9; i >= 0; i--)
    {
        display_buffer[i] = '0' + x % 10;
        x /= 10;
    }

    for(i = 0; i < 9; i++)
    {
        if(display_buffer[i] != '0')
        {
            break;
        }
    }

    for(; i < 10; i++)
    {
        UartPutChar(display_buffer[i]);
    }

}


/**************************************************
	函数功能：发送1字节的数据
	入口参数：x 待发送的数据
	返    回：无
	备    注：无
**************************************************/
void Printc(uint8 x){
	Sending = 1;
	SBUF=x;
	while(Sending);
}


/**************************************************
	函数功能：以HEX格式发送1字节的数据
	入口参数：x 待发送的数据
	返    回：无
	备    注：无
**************************************************/
void PrintHex(uint8 x)
{
	Printc('0');
	Printc('x');
	Printc(HexTable[x>>4]);
	Printc(HexTable[x&0xf]);
	Printc(' ');
}









