#include <STC89C5xRC.H>
#include "key.h"
#include "config.h"


void InitTimer0(void)
{

    TMOD &= 0xF0;
    TMOD |= 0x01;
    ET0 = 1;
    TR0 = 1;

}

void InitKeyboard(void)
{
    KeyIO = 0xFF;
    KeyPress = 0;
    KeyNoChangedTime = 0;
    KeyOld = 0;
    KeyCurrent = 0;
    KeyLast = 0;
    KeyDown = 0;
    KeyUp = 0;
    InitTimer0();
    KeyCanChange = 1;

}



void Timer0Isr(void) interrupt 1
{
    TH0 = (65536 - Fclk / 1000 / 12 * 5 + 15) / 256;
    TL0 = (65536 - Fclk / 1000 / 12 * 5 + 15) % 256;

    if(!KeyCanChange)
    {
        return;
    }

    KeyCurrent = GetKeyValue();

    if(KeyCurrent != KeyOld)
    {
        KeyNoChangedTime = 0;
        KeyOld = KeyCurrent;
        return;
    }
    else
    {
        KeyNoChangedTime++;

        if(KeyNoChangedTime >= 1)
        {
            KeyNoChangedTime = 1;
            KeyPress = KeyOld;
            KeyDown |= (~KeyLast) & (KeyPress);
            KeyUp |= KeyLast & (!KeyPress);
            KeyLast = KeyPress;
        }
    }
}














