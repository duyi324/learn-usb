#include "UsbCore.h"
#include "PDIUSBD12.h"


idata uint8 Buffer[16];  	// 读端点0用的缓冲区


// USB设备描述符的定义
code uint8 DeviceDescriptor[0x12] = //设备描述符为18字节
{
    //bLength字段。设备描述符的长度为18(0x12)字节
    0x12,

    //bDescriptorType字段。设备描述符的编号为0x01
    0x01,

    //bcdUSB字段。这里设置版本为USB1.1，即0x0110。
    //由于是小端结构，所以低字节在先，即0x10，0x01。
    0x10,
    0x01,

    //bDeviceClass字段。我们不在设备描述符中定义设备类，
    //而在接口描述符中定义设备类，所以该字段的值为0。
    0x00,

    //bDeviceSubClass字段。bDeviceClass字段为0时，该字段也为0。
    0x00,

    //bDeviceProtocol字段。bDeviceClass字段为0时，该字段也为0。
    0x00,

    //bMaxPacketSize0字段。PDIUSBD12的端点0大小的16字节。
    0x10,

    //idVender字段。厂商ID号，我们这里取0x8888，仅供实验用。
    //实际产品不能随便使用厂商ID号，必须跟USB协会申请厂商ID号。
    //注意小端模式，低字节在先。
    0x88,
    0x88,

    //idProduct字段。产品ID号，由于是第一个实验，我们这里取0x0001。
    //注意小端模式，低字节应该在前。
    0x01,
    0x00,

    //bcdDevice字段。我们这个USB鼠标刚开始做，就叫它1.0版吧，即0x0100。
    //小端模式，低字节在先。
    0x00,
    0x01,

    //iManufacturer字段。厂商字符串的索引值，为了方便记忆和管理，
    //字符串索引就从1开始吧。
    0x01,

    //iProduct字段。产品字符串的索引值。刚刚用了1，这里就取2吧。
    //注意字符串索引值不要使用相同的值。
    0x02,

    //iSerialNumber字段。设备的序列号字符串索引值。
    //这里取3就可以了。
    0x03,

    //bNumConfigurations字段。该设备所具有的配置数。
    //我们只需要一种配置就行了，因此该值设置为1。
    0x01
};
//////////////////////////设备描述符完毕//////////////////////////////



/********************************************************************
函数功能：延时x毫秒函数。
入口参数：x：延时的毫秒数。
返    回：无。
备    注：无。
********************************************************************/
void DelayXms(uint16 x)
{
    uint16 i;
    uint16 j;

    for(i = 0; i < x; i++)
    {
        for(j = 0; j < 227; j++) {}
    }

}



/********************************************************************
函数功能：USB断开连接函数。
入口参数：无。
返    回：无。
备    注：无。
********************************************************************/
void UsbDisconnect(void)
{
    #ifdef DEBUG0
    Prints("断开USB连接。\r\n");
    #endif
    D12WriteCommand(D12_SET_MODE);
    D12WriteByte(0x06);
    D12WriteByte(0x47);
    DelayXms(1000);
}




void UsbConnect(void)
{
    #ifdef DEBUG0
    Prints("连接USB。\r\n");
    #endif
    D12WriteCommand(D12_SET_MODE);
    D12WriteByte(0x16);
    D12WriteByte(0x47);
}




void UsbBusSuspend()
{
    #ifdef DEBUG0
    Prints("USB总线挂起。\r\n");
    #endif
}


void UsbBusReset()
{
    #ifdef DEBUG0
    Prints("USB总线复位。\r\n");
    #endif
}





/********************************************************************
函数功能：端点0输出中断处理函数。
入口参数：无。
返    回：无。
备    注：无。
********************************************************************/
void UsbEp0Out()
{
    #ifdef DEBUG0
    Prints("USB端点0输出中断。\r\n");
    #endif

    // 读取端点0输出最后传输状态，该操作清除中断标志
    // 并判断第5位是否为1，如果是，则说明是建立包
    if(D12ReadEndpointLastStatus(0) & 0x20)
    {
        D12ReadEndpointBuffer(0, 16, Buffer); //读建立过程数据
        D12AcknowledgeSetup(); //应答建立包
        D12ClearBuffer(); //清缓冲区
    }
		//普通数据输出
    else  //if(D12ReadEndpointLastStatus(0)&0x20)之else
    {
        D12ReadEndpointBuffer(0, 16, Buffer);
        D12ClearBuffer();
    }
}


void UsbEp0In()
{
    #ifdef DEBUG0
    Prints("USB端点0输入中断。\r\n");
    #endif
}


void UsbEp1Out()
{
    #ifdef DEBUG0
    Prints("USB端点1输出中断。\r\n");
    #endif
}


void UsbEp1In()
{
    #ifdef DEBUG0
    Prints("USB端点1输入中断。\r\n");
    #endif
}


void UsbEp2Out()
{
    #ifdef DEBUG0
    Prints("USB端点2输出中断。\r\n");
    #endif
}


void UsbEp2In()
{
    #ifdef DEBUG0
    Prints("USB端点2输入中断。\r\n");
    #endif
}


